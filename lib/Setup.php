<?php
/**
 * Created by PhpStorm.
 * User: gregbueno
 * Date: 11/11/14
 * Time: 5:15 PM
 */

namespace ObservantRecords\WordPress\Themes\EmptyEnsemble2020;

class Setup {

	public function __construct() {

	}

	public static function init() {

		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'wp_enqueue_styles'), 21);
	}

	public static function wp_enqueue_styles() {
		wp_dequeue_style( 'observantrecords2020-style' );

        wp_enqueue_style( 'emptyensemble2020-libre-bodoni-font', '//fonts.googleapis.com/css2?family=Libre+Bodoni:ital,wght@0,400..700;1,400..700&display=swap' );
		wp_enqueue_style( 'emptyensemble2020-istok-font', '//fonts.googleapis.com/css?family=Istok+Web:400,700,700italic,400italic' );
        wp_enqueue_style( 'emptyensemble2020-style', get_stylesheet_directory_uri() . '/assets/css/style.css', array(), null );
	}

}